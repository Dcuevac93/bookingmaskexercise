/* eslint-disable no-param-reassign */
/*
 * Ducks Proposal based
 * https://github.com/erikras/ducks-modular-redux
 *
 */

import produce from 'immer';
import { createActions, handleActions } from 'redux-actions';

// The initial state of the App
export const initialState = {
  loading: false,
  error: false,
  dropdownOpened: false,
  returnFlight: true,
  locations: [],
  bookingData: {
    origin: null,
    destination: null,
    outboundDate: null,
    returnDate: null,
    passengers: {
      adults: 0,
      children: 0,
      infants: 0,
      total() {
        return this.adults + this.children + this.infants;
      },
    },
  },
};

// Options (Prefix and Namespace for actions) - Default Namespace separator '/'
const options = {
  prefix: 'bookingmask/BookingMask', // String used to prefix each type
};

// Actions
export const {
  submitBookingData,
  openDropdown,
  toogleReturnFlight,
  updateOrigin,
  updateDestination,
  updateOutboundDate,
  updateReturnDate,
  updatePassengers,
} = createActions(
  {
    UPDATE_PASSENGERS: (key, val) => ({ key, val }),
  },
  'SUBMIT_BOOKING_DATA',
  'OPEN_DROPDOWN',
  'TOOGLE_RETURN_FLIGHT',
  'UPDATE_ORIGIN',
  'UPDATE_DESTINATION',
  'UPDATE_OUTBOUND_DATE',
  'UPDATE_RETURN_DATE',
  {
    options,
  },
);

// Location Requests Actions
export const {
  locationsRequest,
  locationsSuccess,
  locationsFailure,
} = createActions(
  'LOCATIONS_REQUEST',
  'LOCATIONS_SUCCESS',
  'LOCATIONS_FAILURE',
  {
    options,
  },
);

// Reducer
const reducer = handleActions(
  {
    [openDropdown]: produce((draft, action) => {
      draft.dropdownOpened = action.payload;
    }),
    [toogleReturnFlight]: produce(draft => {
      draft.returnFlight = !draft.returnFlight;
      draft.bookingData.returnDate = null;
    }),
    [updateOrigin]: produce((draft, action) => {
      draft.bookingData.origin = action.payload;
    }),
    [updateDestination]: produce((draft, action) => {
      draft.bookingData.destination = action.payload;
    }),
    [updateOutboundDate]: produce((draft, action) => {
      draft.bookingData.outboundDate = action.payload;
    }),
    [updateReturnDate]: produce((draft, action) => {
      draft.bookingData.returnDate = action.payload;
    }),
    [updatePassengers]: produce((draft, action) => {
      switch (action.payload.key) {
        case 'adults':
          draft.bookingData.passengers.adults = action.payload.val;
          break;
        case 'children':
          draft.bookingData.passengers.children = action.payload.val;
          break;
        case 'infants':
          draft.bookingData.passengers.infants = action.payload.val;
          break;
        default:
          break;
      }
    }),
    [locationsRequest]: produce(draft => {
      draft.loading = true;
    }),
    [locationsSuccess]: produce((draft, action) => {
      draft.loading = false;
      draft.locations = action.payload;
    }),
    [locationsFailure]: produce(draft => {
      draft.loading = false;
      draft.error = true;
    }),
  },
  initialState,
);

export default reducer;
