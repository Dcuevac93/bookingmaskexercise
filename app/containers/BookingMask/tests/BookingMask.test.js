import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import BookingMask from '../index';

const renderer = new ShallowRenderer();

describe('<BookingMask /> Component', () => {
  it('should render and match the snapshot', () => {
    renderer.render(<BookingMask />);
    const renderedOutput = renderer.getRenderOutput();
    expect(renderedOutput).toMatchSnapshot();
  });
});
