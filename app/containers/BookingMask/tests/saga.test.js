/**
 * Tests for Booking Mask sagas
 */

import { put, select } from 'redux-saga/effects';

import { locationsSuccess, locationsFailure } from '../ducks';

import {
  getOrigin,
  getDestination,
  getOutboundDate,
  // getReturnDate,
  getReturnFlight,
  getAdultsCount,
  getChildrenCount,
  getInfantsCount,
} from '../selectors';

import { getLocations, sendBookingData } from '../saga';

const error = { message: 'Oh snap!' };

describe('BookingMask Saga', () => {
  describe('getLocations fn', () => {
    let country;
    let locations;
    beforeEach(() => {
      country = 'United States';
      locations = [
        {
          name: 'Afghanistan',
          alpha3Code: 'AFG',
        },
        {
          name: 'France',
          alpha3Code: 'FRA',
        },
      ];
    });

    it('should dispatch the locationsSuccess action if it requests the data successfully', () => {
      const sagafn = getLocations({ payload: { search: country } });
      expect(sagafn.next().value).toMatchSnapshot();
      expect(sagafn.next(locations).value).toEqual(
        put(locationsSuccess(locations)),
      );
    });

    it('should call the repoLoadingError action if the response errors', () => {
      const sagafn = getLocations({ payload: { search: country } });
      expect(sagafn.next().value).toMatchSnapshot();
      expect(sagafn.throw(error).value).toEqual(put(locationsFailure(error)));
    });
  });

  describe('sendBookingData fn', () => {
    const sagafn = sendBookingData();

    it('should take all booking data and build an url', () => {
      expect(sagafn.next().value).toEqual(select(getOrigin));
      expect(sagafn.next().value).toEqual(select(getDestination));
      expect(sagafn.next().value).toEqual(select(getOutboundDate));
      expect(sagafn.next().value).toEqual(select(getReturnFlight));
      expect(sagafn.next().value).toEqual(select(getAdultsCount));
      expect(sagafn.next().value).toEqual(select(getChildrenCount));
      expect(sagafn.next().value).toEqual(select(getInfantsCount));
    });
  });
});
