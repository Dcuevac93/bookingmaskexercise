/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React from 'react';
import Routes from 'routes';
import { Layout } from 'antd';

export default function App() {
  return (
    <Layout>
      <Layout.Content>
        <Routes />
      </Layout.Content>
    </Layout>
  );
}
