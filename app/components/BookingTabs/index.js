export { default as Tabs } from './Tabs';
export { default as Tab } from './Tab';
export { default as TabList } from './TabList';
export { default as TabPanel } from './TabPanel';
export { default as TabLink } from './TabLink';
export { default as TabText } from './TabText';
