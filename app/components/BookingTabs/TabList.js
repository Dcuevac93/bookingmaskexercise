import styled from 'styled-components';
import { TabList } from 'react-tabs';

export const TabListStyled = styled(TabList)`
  display: flex;
  align-items: stretch; /* Default */
  justify-content: space-between;
  border-bottom: 1px solid #aaa;
  margin: 0;
  padding: 0;
  width: 100%;
  @media only screen and (max-width: 1150px) {
    flex-wrap: wrap;
  }
`;

export default TabListStyled;
